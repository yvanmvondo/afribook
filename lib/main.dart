import 'package:flutter/material.dart';
import 'package:library_app/pages/bienvenue/bienvenue.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Library app',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Bienvenue()
    );
  }
}

